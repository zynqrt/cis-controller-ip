
#ifndef CISCONTROLLER_H
#define CISCONTROLLER_H


/****************** Include Files ********************/
#include "xil_types.h"
#include "xstatus.h"

#define CISCONTROLLER_S00_AXI_CTRL_REG_CR0_OFFSET   0
#define CISCONTROLLER_S00_AXI_CTRL_REG_CR1_OFFSET   4
#define CISCONTROLLER_S00_AXI_CTRL_REG_CR2_OFFSET   8
#define CISCONTROLLER_S00_AXI_CTRL_REG_CR3_OFFSET   12
#define CISCONTROLLER_S00_AXI_CTRL_REG_CR4_OFFSET   16
#define CISCONTROLLER_S00_AXI_CTRL_REG_LINES_OFFSET 20
#define CISCONTROLLER_S00_AXI_CTRL_REG_IP_OFFSET    24
#define CISCONTROLLER_S00_AXI_CTRL_REG_AP3_OFFSET   28
#define CISCONTROLLER_S00_AXI_CTRL_REG_AP6_OFFSET   32
#define CISCONTROLLER_S00_AXI_CTRL_REG_AP12_OFFSET  36
#define CISCONTROLLER_S00_AXI_CTRL_REG_APC_OFFSET   40


/**
 * Регистр CR0
 * Регистр настройки тактовой частоты ADC
 * FALLTIME - тик, при котором ADC_CLK переходит в 0
 * RISETIME - тик, при котором ADC_CLK переходит в 1
 * ST0, ST1 - тики, при которых забираются данные из АЦП
 * PRBS - использовать ПСП как источник данных
 * ENABLE - включить клоки
 */
#define CISCONTROLLER_REG_CR0_FALLTIME(t) ((t & 0b11111) << 0)
#define CISCONTROLLER_REG_CR0_RISETIME(t) ((t & 0b11111) << 5)
#define CISCONTROLLER_REG_CR0_ST0(t) ((t & 0b11111) << 10)
#define CISCONTROLLER_REG_CR0_ST1(t) ((t & 0b11111) << 15)
#define CISCONTROLLER_REG_CR0_PRBS(e) ((e & 0b1) << 20)
#define CISCONTROLLER_REG_CR0_ENABLE(e) ((e & 0b1) << 21)


/**
 * Регистр CR1
 * Регистр настройки сигнала CDSCLK1
 * PHASE - фаза перехода CDSCLK1 в 1
 * WIDTH - длительность сигнала CDSCLK1
 * SKIP - пропус тактов ADC_CDSCKL
 * ENABLE - включить сигнал
 */
#define CISCONTROLLER_REG_CR1_PHASE(p)  ((p & 0b11111) << 0)
#define CISCONTROLLER_REG_CR1_WIDTH(w)  ((w & 0b11111) << 5)
#define CISCONTROLLER_REG_CR1_SKIP(s) ((s & 0b1) << 11)
#define CISCONTROLLER_REG_CR1_ENABLE(e) ((e & 0b1) << 10)


/**
 * Регистр CR2
 * Регистр настройки сигнала CDSCLK2
 * PHASE - фаза перехода CDSCLK2 в 1
 * WIDTH - длительность сигнала CDSCLK2
 * SKIP - пропус тактов ADC_CDSCKL
 * ENABLE - включить сигнал
 */
#define CISCONTROLLER_REG_CR2_PHASE(p)  ((p & 0b11111) << 0)
#define CISCONTROLLER_REG_CR2_WIDTH(w)  ((w & 0b11111) << 5)
#define CISCONTROLLER_REG_CR2_ENABLE(e) ((e & 0b1) << 10)
#define CISCONTROLLER_REG_CR2_ENABLE(e) ((e & 0b1) << 10)


/**
 * Регистр CR3 - Настройка CIS
 * SI_PHASE - фаза сигнала SI
 * CLOCK_PHASE - фаза CIS_CLK относительно фазы ADC_CLK
 * SI_WIDTH - длительность сигнала SI
 */
#define CISCONTROLLER_REG_CR3_SI_PHASE(p)    ((p & 0b111111) << 2)
#define CISCONTROLLER_REG_CR3_CLOCK_PHASE(p) ((p & 0b11111)  << 8)
#define CISCONTROLLER_REG_CR3_SI_WIDTH(w)    ((w & 0b111111) << 13)


/**
 * Регистр CR4 
 * READLINEONE - Чтение одной строки. По фронту
 * READLINEMANY - Чтение нескольких строк, по уровню
 * RESOLUTION - разрешение сканера
 */
#define CISCONTROLLER_REG_CR4_READLINEONE  (1 << 0)
#define CISCONTROLLER_REG_CR4_READLINEMANY (1 << 1)
#define CISCONTROLLER_REG_CR4_RESOLUTION(r) ((r & 0b11) << 2)


/**************************** Type Definitions *****************************/
/**
 *
 * Write a value to a CISCONTROLLER register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the CISCONTROLLERdevice.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void CISCONTROLLER_mWriteReg(u32 BaseAddress, unsigned RegOffset, u32 Data)
 *
 */
#define CISCONTROLLER_mWriteReg(BaseAddress, RegOffset, Data) \
  	Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))

/**
 *
 * Read a value from a CISCONTROLLER register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the CISCONTROLLER device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	u32 CISCONTROLLER_mReadReg(u32 BaseAddress, unsigned RegOffset)
 *
 */
#define CISCONTROLLER_mReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))

/************************** Function Prototypes ****************************/
/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the CISCONTROLLER instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus CISCONTROLLER_Reg_SelfTest(void * baseaddr_p);

#endif // CISCONTROLLER_H
