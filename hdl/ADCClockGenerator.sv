`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.04.2020 21:27:48
// Design Name: 
// Module Name: ADCReader
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ADCClockGenerator #
    (
        parameter integer C_REGISTER_CR0_WIDTH = 32,
        parameter integer C_REGISTER_CR1_WIDTH = 32,
        parameter integer C_REGISTER_CR2_WIDTH = 32,
        parameter integer C_REGISTER_CR3_WIDTH = 32
    )
    (
        input wire aclk,
        input wire aresetn,

        input wire [C_REGISTER_CR0_WIDTH - 1 : 0] register_cr0,
        input wire [C_REGISTER_CR1_WIDTH - 1 : 0] register_cr1,
        input wire [C_REGISTER_CR2_WIDTH - 1 : 0] register_cr2,
        input wire [C_REGISTER_CR3_WIDTH - 1 : 0] register_cr3,

        output wire cis_clk_toggle,
        output wire cis_clk_skip_test,

        output wire st0,
        output wire st1,

        output wire ADC_CLK,
        output wire ADC_CDSCLK1,
        output wire ADC_CDSCLK2
    );


wire [4:0] cr0_falltime = register_cr0[4:0];
wire [4:0] cr0_risetime = register_cr0[9:5];
wire [4:0] cr0_sample_time0 = register_cr0[14:10];
wire [4:0] cr0_sample_time1 = register_cr0[19:15];
wire cr0_enable = register_cr0[21];


wire [4:0] cr1_cdsclk1_phase = register_cr1[4:0];
wire [4:0] cr1_cdsclk1_width = register_cr1[9:5];
wire cr1_cdsclk1_enable = register_cr1[10];
wire cr1_cdsclk1_skip = register_cr1[11];


wire [4:0] cr2_cdsclk2_phase = register_cr2[4:0];
wire [4:0] cr2_cdsclk2_width = register_cr2[9:5];
wire cr2_cdsclk2_enable = register_cr2[10];
wire cr2_cdsclk2_skip = register_cr2[11];

wire [4:0] cr3_clock_phase = register_cr3[12:8];


(* mark_debug = "true" *)
bit [4:0] full_bit_cnt;
    
    // bit counter
    always_ff @(posedge aclk) begin
        if (~aresetn) begin
            full_bit_cnt <= 0;
        end
        else begin
            if (full_bit_cnt < cr0_risetime)
                full_bit_cnt <= full_bit_cnt + 1'b1;
            else
                full_bit_cnt <= 0;
        end
    end

    // Генератор тактовой частоты
bit adc_clk;
    always_ff @(posedge aclk) begin
        if (~aresetn) begin
            adc_clk <= 0;
        end
        else begin
            if (full_bit_cnt == cr0_risetime)
                adc_clk <= 1'b1;
            if (full_bit_cnt == cr0_falltime)
                adc_clk <= 1'b0;
        end
    end

assign ADC_CLK = adc_clk & cr0_enable;

assign cis_clk_toggle = (full_bit_cnt == cr3_clock_phase);

assign st0 = (full_bit_cnt == cr0_sample_time0);
assign st1 = (full_bit_cnt == cr0_sample_time1);
wire cdsclk2_start = (full_bit_cnt == cr2_cdsclk2_phase);
    
    bit cdsclk2_skip;
    always_ff @(posedge aclk) begin
        if (~aresetn) begin
            cdsclk2_skip <= 0;
        end
        else begin
            if (cdsclk2_start)
                cdsclk2_skip <= ~cdsclk2_skip; // Каждый второй CDSCLK2 выдаём наружу
        end
    end
assign cis_clk_skip_test = cdsclk2_skip;

    // Генератор CDSCLK2
    CDSCLKGenerator # (
        .C_COUNTER_WIDTH(5)
    ) generator_cds2 (
        .aclk    (aclk),
        .aresetn (aresetn),
        .start   (cdsclk2_start & (cdsclk2_skip ^ cr2_cdsclk2_skip)), 
        .width   (cr2_cdsclk2_width),
        .enable  (cr2_cdsclk2_enable),
        .CDSCLK  (ADC_CDSCLK2)
    );
        
    // Генератор CDSCLK1
    CDSCLKGenerator # (
        .C_COUNTER_WIDTH(5)
    ) generator_cds1 (
        .aclk   (aclk),
        .aresetn(aresetn),
        .start  (full_bit_cnt == cr1_cdsclk1_phase),
        .width  (cr1_cdsclk1_width),
        .enable (cr1_cdsclk1_enable),
        .CDSCLK (ADC_CDSCLK1)
    );

endmodule



/* ======================================================================= */
module CDSCLKGenerator # 
    (
        parameter C_COUNTER_WIDTH = 5
    )
    (
        input wire aclk,
        input wire aresetn,

        input wire start, // Стартовый импульс
        input wire [C_COUNTER_WIDTH - 1 : 0] width,
        input wire enable,
        output wire CDSCLK
    );


(* mark_debug = "true" *)
bit [C_COUNTER_WIDTH - 1 : 0] width_cnt;

(* mark_debug = "true" *)
enum bit [0:0] {
    IDLE = 'h0,
    COUNTING
} state;
    
bit cdsclk;
    always_ff @(posedge aclk) begin
        if (~aresetn) begin
            state <= IDLE;
            cdsclk <= 0;
        end
        else begin
            case (state)
                IDLE: begin
                    width_cnt <= 0;
                    cdsclk <= 0;
                    if (start & enable)
                        state <= COUNTING;
                end

                COUNTING: begin
                    if (width_cnt == (width - 1'b1) || start)
                        state <= IDLE;
                    width_cnt <= width_cnt + 1;
                    cdsclk <= 1 & (|width) & enable;
                end
            endcase // state
        end
    end

/*
    Не работает

    always_ff @(posedge aclk)
        if (~aresetn)
            state <= IDLE;
        else
            state <= next;

    always @(*) begin
        case (state)
            IDLE: begin
                if (start & enable)
                    next = COUNTING;
            end

            COUNTING: begin
                if (width_cnt == (width - 1'b1) || start)
                    next = IDLE;
            end
        endcase
    end

bit cdsclk;
    always_ff @(posedge aclk) begin
        if (~aresetn) begin
            width_cnt <= 0;
            cdsclk <= 0;
        end
        else begin
            case (state)
                IDLE: begin
                    width_cnt <= 0;
                    cdsclk <= 0;
                end

                COUNTING: begin
                    width_cnt <= width_cnt + 1;
                    cdsclk <= 1 & (|width) & enable;
                end
            endcase
        end
    end
*/
assign CDSCLK = cdsclk;

endmodule
