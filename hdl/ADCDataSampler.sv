module ADCDataSampler #
    (
        parameter integer C_ADC_DATA_WIDTH = 8,
        parameter integer C_M00_AXIS_TDATA_WIDTH = 32,
        parameter integer C_REGISTER_LINES_WIDTH = 32
    )
	(
		input wire aclk,
		input wire aresetn,

		input wire read_line,
		input wire read_line_many,
		
		input wire cis_si,
		input wire active_line_end,


		input wire st0,
		input wire st1,
		input wire [C_ADC_DATA_WIDTH - 1 : 0] ADC_DATA,

		input wire [11:0] full_pixel_count,
		input wire [C_REGISTER_LINES_WIDTH-1:0] register_lines,
		output reg register_lines_decrement,

		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready
	); 



wire read_line_rise;
	edge_detect ed_rl_r(
		.clk (aclk),
		.in  (read_line),
		.rise(read_line_rise)
	);

wire si_low;
	edge_detect ed_si_f (
		.clk (aclk),
		.in  (cis_si),
		.fall(si_low)
	);

	enum bit [3:0] {
		IDLE,
		WAIT_SI_LOW,
		READING,
		DUMMY
	} state;

// Выкинуть bit_cnt
	always_ff @(posedge aclk) begin
		if (~aresetn) begin
			state <= IDLE;
		end
		else begin
			register_lines_decrement <= 1'b0;
			case (state)
				IDLE: begin
					if ((read_line_rise | read_line_many) && (register_lines > 0)) begin
						state <= WAIT_SI_LOW;
					end
				end

				WAIT_SI_LOW: begin
					if (si_low) begin
						state <= READING;
					end
				end
				
				READING: begin
					if (active_line_end & st0) begin
						state <= DUMMY;
						register_lines_decrement <= 1'b1;
					end
				end

				// Пустое состояние на 1 такт, чтобы декрементировать счетчик строк и не сканировать лишнюю строку при read_line_many
				DUMMY: begin
					state <= IDLE;
				end
			endcase // state
		end
	end


wire wr_en = (st0 | st1) & (state == READING);
wire fifo_reset = (~aresetn) | cis_si;

wire fifo_full;
wire fifo_almost_full;
wire fifo_empty;
wire fifo_almost_empty;


	fifo8x32 fifo_i (
		.clk(aclk),                      // input wire clk
		.srst(fifo_reset),                    // input wire srst
		
		.din(ADC_DATA),                      // input wire [7 : 0] din
		.wr_en(wr_en),                  // input wire wr_en
		
		.rd_en(m00_axis_tready),                  // input wire rd_en
		.dout(m00_axis_tdata),                    // output wire [31 : 0] dout
		
		.full(fifo_full),                    // output wire full
		.almost_full(fifo_almost_full),      // output wire almost_full
		
		.empty(fifo_empty),                  // output wire empty
		.almost_empty(fifo_almost_empty)
	);

assign m00_axis_tvalid = ~fifo_empty;
assign m00_axis_tstrb = {(C_M00_AXIS_TDATA_WIDTH/8){1'b1}};

(* mark_debug = "true" *)
bit [11:0] read_counter;
	always_ff @(posedge aclk) begin
		if (fifo_reset) begin
			read_counter <= 0;
		end
		else begin
			if (m00_axis_tready & m00_axis_tvalid)
				read_counter <= read_counter + 1;
		end
	end

assign m00_axis_tlast = (read_counter == full_pixel_count) & (register_lines == '0);
endmodule
