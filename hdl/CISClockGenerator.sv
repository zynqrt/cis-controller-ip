`timescale 1ns / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.04.2020 21:29:18
// Design Name: 
// Module Name: CISClockGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CISClockGenerator #
    (
        parameter integer C_REGISTER_CR3_WIDTH = 32,
        parameter integer C_REGISTER_CR4_WIDTH = 32,

        parameter integer ACTIVE_PIXELS_1200DPI = 144, //2592;
        parameter integer ACTIVE_PIXELS_600DPI  =  74, //1296;
        parameter integer ACTIVE_PIXELS_300DPI  =  36, //648;
        parameter integer INACTIVE_PIXELS = 12  // 124;
    )
    (
        input wire aclk,
        input wire aresetn,

        input wire [C_REGISTER_CR3_WIDTH - 1 : 0] register_cr3,
        input wire [C_REGISTER_CR4_WIDTH - 1 : 0] register_cr4,

        input wire cis_clk_toggle,
        input wire cis_clk_skip_test,
        
        (* mark_debug = "true" *)
        output bit [11:0] full_pixel_count,
        output bit active_line_end,

        output wire CIS_CLK,
        output wire CIS_SI,
        output wire CIS_SEL
    );

    localparam [1:0] CIS_RESOLUTION_INVALID = 2'b00;
    localparam [1:0] CIS_RESOLUTION_300 = 2'b01;
    localparam [1:0] CIS_RESOLUTION_600 = 2'b10;
    localparam [1:0] CIS_RESOLUTION_1200 = 2'b11;

    localparam integer PIXEL_CNT_WIDTH = 12;
    
    // Количество пикселей для формирования 0.2, 0.4 и 0.8 мс на строку.
    // Учитываются неактивные пиксели, активные и "хвост" после активных
    localparam integer MAX_PIXELS_1200DPI = 3200;   // 3200; //Для 0,8 мс на линию при 4 МГц
    localparam integer MAX_PIXELS_600DPI  = 1600;   // 1600;
    localparam integer MAX_PIXELS_300DPI  =  800;   //  800;


wire [1:0] cr4_resolution = register_cr4[3:2];
wire [5:0] cr3_si_phase = register_cr3[7:2];
wire [4:0] cr3_clock_phase = register_cr3[12:8];
wire [5:0] cr3_si_width = register_cr3[18:13];
// wire [PIXEL_CNT_WIDTH - 1 : 0] cr3_active_pixels = register_cr3[PIXEL_CNT_WIDTH-1+19 : 19]

    
wire config_valid = ~(cr4_resolution == CIS_RESOLUTION_INVALID);
bit cis_clk;
    always_ff @(posedge aclk) begin
        if (~aresetn) begin
            cis_clk <= 1'b0;
        end
        else begin
            if (config_valid) begin
                if (cis_clk_toggle)                
                    cis_clk <= ~cis_clk;
            end
            else begin
                cis_clk <= 0;
            end
        end
    end
assign CIS_CLK = cis_clk;

wire clock_rise = cis_clk_toggle & (~cis_clk);


    // Генератор длительности между SI
    bit [11:0] max_pixels;
    always @(*) begin
        case (cr4_resolution)
            CIS_RESOLUTION_INVALID: max_pixels <= 0;
            CIS_RESOLUTION_300:     max_pixels <= MAX_PIXELS_300DPI - 1;
            CIS_RESOLUTION_600:     max_pixels <= MAX_PIXELS_600DPI - 1;
            CIS_RESOLUTION_1200:    max_pixels <= MAX_PIXELS_1200DPI - 1; 
        endcase // cr3_resolution
    end

(* mark_debug = "true" *)
bit [PIXEL_CNT_WIDTH - 1 : 0] pixel_cnt;
    always_ff @(posedge aclk) begin
        if (~aresetn) begin
            pixel_cnt <= 0;
        end
        else begin
            if (config_valid) begin
                if (clock_rise) begin
                    if (pixel_cnt < max_pixels)
                        pixel_cnt <= pixel_cnt + 1;
                    else
                        pixel_cnt <= 0;
                end
            end
            else begin
                pixel_cnt <= 0;
            end
        end
    end


    // Запись идет со второго пикселя.
    // В итоговый FIFO попадет (INACTIVE + ACTIVE - 2)*C*B, C - количество каналов (2), B - АЦП передает по 2 отсчета на такт.
    // (INACTIVE + ACTIVE - 2)*2*2. В FIFO всегда будет кратное 32 битам значение.
    always @(*) begin
        case (cr4_resolution)
            CIS_RESOLUTION_INVALID: begin
                active_line_end <= 1;
                full_pixel_count <= 0;
            end

            CIS_RESOLUTION_300: begin
                active_line_end <= (pixel_cnt == (INACTIVE_PIXELS + ACTIVE_PIXELS_300DPI));
                full_pixel_count <= (INACTIVE_PIXELS + ACTIVE_PIXELS_300DPI - 2 - 1);
            end

            CIS_RESOLUTION_600: begin
                active_line_end <= (pixel_cnt == (INACTIVE_PIXELS + ACTIVE_PIXELS_600DPI)); // 84 + 1
                full_pixel_count <= (INACTIVE_PIXELS + ACTIVE_PIXELS_600DPI - 2 - 1);
            end

            CIS_RESOLUTION_1200: begin
                active_line_end <= (pixel_cnt == (INACTIVE_PIXELS + ACTIVE_PIXELS_1200DPI));
                full_pixel_count <= (INACTIVE_PIXELS + ACTIVE_PIXELS_1200DPI - 2 - 1);
            end
        endcase
    end

    // SI generator
bit cis_si;
(* mark_debug = "true" *)
bit [5:0] si_cnt;
(* mark_debug = "true" *)
enum bit [0:0] {
    SI_IDLE,
    SI_COUNTING
} si_cnt_state;

    always_ff @(posedge aclk) begin
        if (~aresetn) begin
            si_cnt <= 0;
            si_cnt_state <= SI_IDLE;
            cis_si <= 0;
        end
        else begin
            if (config_valid) begin
                if (clock_rise & (pixel_cnt == 0)) begin
                    si_cnt <= 0;
                    si_cnt_state <= SI_COUNTING;
                    cis_si <= 0;
                end
                else begin
                    if (si_cnt_state == SI_COUNTING) begin
                        si_cnt <= si_cnt + 1;
                        if (&si_cnt)
                            si_cnt_state <= SI_IDLE;

                        if (si_cnt == cr3_si_phase)
                            cis_si <= 1;
                        if (si_cnt == (cr3_si_phase + cr3_si_width))
                            cis_si <= 0;
                    end
                end
            end
            else begin
                si_cnt <= 0;
                cis_si <= 0;
                si_cnt_state <= SI_IDLE;
            end
        end
    end
assign CIS_SI = cis_si;
    
    
    // Selection signal generator

bit cis_sel;
    always_ff @(posedge aclk) begin
        if (~aresetn) begin
            cis_sel <= 0;
        end
        else begin
            case (cr4_resolution)
                CIS_RESOLUTION_INVALID: cis_sel <= 0;
                CIS_RESOLUTION_300:  cis_sel <= (pixel_cnt > 10);
                CIS_RESOLUTION_600:  cis_sel <= 1;
                CIS_RESOLUTION_1200: cis_sel <= 0;
            endcase // cr3_resolution
        end
    end

assign CIS_SEL = cis_sel;


endmodule
