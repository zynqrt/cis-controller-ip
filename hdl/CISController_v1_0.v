
`timescale 1 ns / 1 ps

	module CISController_v1_0 #
	(
		// Users to add parameters here
		parameter integer C_ADC_DATA_WIDTH = 8,
		
		parameter integer INACTIVE_PIXELS       =  124,  // 124;
		parameter integer ACTIVE_PIXELS_1200DPI = 2594, //2592;
        parameter integer ACTIVE_PIXELS_600DPI  = 1298, //1296;
        parameter integer ACTIVE_PIXELS_300DPI  =  650, //648;
        // (INACTIVE + ACTIVE - 2)/4 должно быть кратно 16

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI_CTRL
		parameter integer C_S00_AXI_CTRL_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_CTRL_ADDR_WIDTH	= 8,

		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32
	)
	(
		// Users to add ports here
		output wire ADC_CLK,
		output wire ADC_CDSCLK1,
		output wire ADC_CDSCLK2,
		input wire [C_ADC_DATA_WIDTH - 1 : 0] ADC_DATA,

		output wire CIS_CLK,
		output wire CIS_SI,
		output wire CIS_SEL,

		input wire READLINEONE,
		input wire READLINEMANY,

		output wire irq,

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI_CTRL
		input wire  s00_axi_ctrl_aclk,
		input wire  s00_axi_ctrl_aresetn,
		input wire [C_S00_AXI_CTRL_ADDR_WIDTH-1 : 0] s00_axi_ctrl_awaddr,
		input wire [2 : 0] s00_axi_ctrl_awprot,
		input wire  s00_axi_ctrl_awvalid,
		output wire  s00_axi_ctrl_awready,
		input wire [C_S00_AXI_CTRL_DATA_WIDTH-1 : 0] s00_axi_ctrl_wdata,
		input wire [(C_S00_AXI_CTRL_DATA_WIDTH/8)-1 : 0] s00_axi_ctrl_wstrb,
		input wire  s00_axi_ctrl_wvalid,
		output wire  s00_axi_ctrl_wready,
		output wire [1 : 0] s00_axi_ctrl_bresp,
		output wire  s00_axi_ctrl_bvalid,
		input wire  s00_axi_ctrl_bready,
		input wire [C_S00_AXI_CTRL_ADDR_WIDTH-1 : 0] s00_axi_ctrl_araddr,
		input wire [2 : 0] s00_axi_ctrl_arprot,
		input wire  s00_axi_ctrl_arvalid,
		output wire  s00_axi_ctrl_arready,
		output wire [C_S00_AXI_CTRL_DATA_WIDTH-1 : 0] s00_axi_ctrl_rdata,
		output wire [1 : 0] s00_axi_ctrl_rresp,
		output wire  s00_axi_ctrl_rvalid,
		input wire  s00_axi_ctrl_rready,

		// Ports of Axi Slave Bus Interface S00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready
	);

	localparam integer C_REGISTER_CR0_WIDTH = 22;
    localparam integer C_REGISTER_CR1_WIDTH = 12;
    localparam integer C_REGISTER_CR2_WIDTH = 12;
    localparam integer C_REGISTER_CR3_WIDTH = 19;
    localparam integer C_REGISTER_CR4_WIDTH = 4;
    localparam integer C_REGISTER_LINES_WIDTH = 16;
    localparam integer C_INTERRUPT_WIDTH = 2;


	wire [C_REGISTER_CR0_WIDTH - 1 : 0] register_cr0;
	wire [C_REGISTER_CR1_WIDTH - 1 : 0] register_cr1;
	wire [C_REGISTER_CR2_WIDTH - 1 : 0] register_cr2;
	wire [C_REGISTER_CR3_WIDTH - 1 : 0] register_cr3;
	wire [C_REGISTER_CR4_WIDTH - 1 : 0] register_cr4;
    wire [C_REGISTER_LINES_WIDTH - 1 : 0] register_lines;
    wire register_lines_decrement;
    
    wire [C_INTERRUPT_WIDTH - 1 : 0] INTERRUPT_ENABLE;
    wire [C_INTERRUPT_WIDTH - 1 : 0] INTERRUPT_ACK;
    wire [C_INTERRUPT_WIDTH - 1 : 0] INTERRUPT_STATUS;
    wire 							 GIER;


// Instantiation of Axi Bus Interface S00_AXI_CTRL
	CISController_v1_0_S00_AXI_CTRL # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_CTRL_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_CTRL_ADDR_WIDTH),
		.C_REGISTER_CR0_WIDTH(C_REGISTER_CR0_WIDTH),
		.C_REGISTER_CR1_WIDTH(C_REGISTER_CR1_WIDTH),
		.C_REGISTER_CR2_WIDTH(C_REGISTER_CR2_WIDTH),
		.C_REGISTER_CR3_WIDTH(C_REGISTER_CR3_WIDTH),
		.C_REGISTER_CR4_WIDTH(C_REGISTER_CR4_WIDTH),
        .C_REGISTER_LINES_WIDTH(C_REGISTER_LINES_WIDTH),
        .C_INTERRUPT_WIDTH(C_INTERRUPT_WIDTH)
	) CISController_v1_0_S00_AXI_CTRL_inst (
		.S_AXI_ACLK(s00_axi_ctrl_aclk),
		.S_AXI_ARESETN(s00_axi_ctrl_aresetn),
		.S_AXI_AWADDR(s00_axi_ctrl_awaddr),
		.S_AXI_AWPROT(s00_axi_ctrl_awprot),
		.S_AXI_AWVALID(s00_axi_ctrl_awvalid),
		.S_AXI_AWREADY(s00_axi_ctrl_awready),
		.S_AXI_WDATA(s00_axi_ctrl_wdata),
		.S_AXI_WSTRB(s00_axi_ctrl_wstrb),
		.S_AXI_WVALID(s00_axi_ctrl_wvalid),
		.S_AXI_WREADY(s00_axi_ctrl_wready),
		.S_AXI_BRESP(s00_axi_ctrl_bresp),
		.S_AXI_BVALID(s00_axi_ctrl_bvalid),
		.S_AXI_BREADY(s00_axi_ctrl_bready),
		.S_AXI_ARADDR(s00_axi_ctrl_araddr),
		.S_AXI_ARPROT(s00_axi_ctrl_arprot),
		.S_AXI_ARVALID(s00_axi_ctrl_arvalid),
		.S_AXI_ARREADY(s00_axi_ctrl_arready),
		.S_AXI_RDATA(s00_axi_ctrl_rdata),
		.S_AXI_RRESP(s00_axi_ctrl_rresp),
		.S_AXI_RVALID(s00_axi_ctrl_rvalid),
		.S_AXI_RREADY(s00_axi_ctrl_rready),

		.register_cr0 (register_cr0),
		.register_cr1 (register_cr1),
		.register_cr2 (register_cr2),
		.register_cr3 (register_cr3),
		.register_cr4 (register_cr4),
        .register_lines                (register_lines),
        .register_lines_decrement      (register_lines_decrement),
		.register_inactive_pixels      (INACTIVE_PIXELS),
		.register_active_pixels_300    (ACTIVE_PIXELS_300DPI),
		.register_active_pixels_600    (ACTIVE_PIXELS_600DPI),
		.register_active_pixels_1200   (ACTIVE_PIXELS_1200DPI),
		.register_active_pixels_current(0),
		
		.INTERRUPT_ENABLE              (INTERRUPT_ENABLE),  // output
		.INTERRUPT_ACK                 (INTERRUPT_ACK),     // output
		.INTERRUPT_STATUS              (INTERRUPT_STATUS),  // input
		.GIER                          (GIER)               // output
	);

	// Add user logic here

	wire cis_clk_toggle;
	wire cis_clk_skip_test;
	wire read_line;
	wire active_line_end;

	wire st0;
	wire st1;

	ADCClockGenerator #	(
		.C_REGISTER_CR0_WIDTH(C_REGISTER_CR0_WIDTH),
		.C_REGISTER_CR1_WIDTH(C_REGISTER_CR1_WIDTH),
		.C_REGISTER_CR2_WIDTH(C_REGISTER_CR2_WIDTH),
		.C_REGISTER_CR3_WIDTH(C_REGISTER_CR3_WIDTH)
	) ADCClockGenerator_inst (
		.aclk       (s00_axi_ctrl_aclk),
		.aresetn    (s00_axi_ctrl_aresetn),
		
		.register_cr0(register_cr0),
		.register_cr1(register_cr1),
		.register_cr2(register_cr2),
		.register_cr3  (register_cr3),
		
		.cis_clk_toggle(cis_clk_toggle),
		.cis_clk_skip_test(cis_clk_skip_test),
		
		// Импульсы захвата данных из АЦП
		.st0              (st0), 
		.st1              (st1),

		.ADC_CLK    (ADC_CLK),
		.ADC_CDSCLK1(ADC_CDSCLK1),
		.ADC_CDSCLK2(ADC_CDSCLK2)
	);

	wire [11:0] full_pixel_count;

    CISClockGenerator # (
    	.C_REGISTER_CR3_WIDTH (C_REGISTER_CR3_WIDTH),
    	.C_REGISTER_CR4_WIDTH (C_REGISTER_CR4_WIDTH),
    	.INACTIVE_PIXELS      (INACTIVE_PIXELS),
    	.ACTIVE_PIXELS_300DPI (ACTIVE_PIXELS_300DPI),
    	.ACTIVE_PIXELS_600DPI (ACTIVE_PIXELS_600DPI),
    	.ACTIVE_PIXELS_1200DPI(ACTIVE_PIXELS_1200DPI)
    ) CISClockGenerator_i (
    	.aclk        (s00_axi_ctrl_aclk),
    	.aresetn     (s00_axi_ctrl_aresetn),
    	.register_cr3(register_cr3),
    	.register_cr4(register_cr4),

    	.cis_clk_toggle(cis_clk_toggle),
    	.cis_clk_skip_test(cis_clk_skip_test),
    	.active_line_end  (active_line_end),
    	
    	.full_pixel_count (full_pixel_count),

    	.CIS_CLK     (CIS_CLK),
    	.CIS_SI      (CIS_SI),
    	.CIS_SEL     (CIS_SEL)
    );

    wire [7:0] prbs_out;
    wire cr0_test_data = register_cr0[20];
    wire [C_ADC_DATA_WIDTH - 1 : 0] input_data;
    assign input_data = cr0_test_data ? prbs_out : ADC_DATA;

    PRBS # (
    	.PRBS_OUT_WIDTH(C_ADC_DATA_WIDTH)
    ) prbs_i (
    	.aclk    (s00_axi_ctrl_aclk),
    	.aresetn (s00_axi_ctrl_aresetn),
    	.cis_si  (CIS_SI),
    	.adc_clk (ADC_CLK),
    	.prbs_out(prbs_out)
    );

    wire READLINEONE_sync;
	xpm_cdc_single #(
		.DEST_SYNC_FF(2),   // DECIMAL; range: 2-10
		.INIT_SYNC_FF(0),   // DECIMAL; 0=disable simulation init values, 1=enable simulation init values
		.SIM_ASSERT_CHK(0), // DECIMAL; 0=disable simulation messages, 1=enable simulation messages
		.SRC_INPUT_REG(0)   // DECIMAL; 0=do not register input, 1=register input
	) xpm_cdc_single_inst0 (
		.dest_out(READLINEONE_sync), // 1-bit output: src_in synchronized to the destination clock domain. This output is
                           // registered.

		.dest_clk(s00_axi_ctrl_aclk), // 1-bit input: Clock signal for the destination clock domain.
		.src_clk(1'b0),   // 1-bit input: optional; required when SRC_INPUT_REG = 1
		.src_in(READLINEONE)      // 1-bit input: Input signal to be synchronized to dest_clk domain.
	);

	wire READLINEMANY_sync;
	xpm_cdc_single #(
		.DEST_SYNC_FF(2),   // DECIMAL; range: 2-10
		.INIT_SYNC_FF(0),   // DECIMAL; 0=disable simulation init values, 1=enable simulation init values
		.SIM_ASSERT_CHK(0), // DECIMAL; 0=disable simulation messages, 1=enable simulation messages
		.SRC_INPUT_REG(0)   // DECIMAL; 0=do not register input, 1=register input
	) xpm_cdc_single_inst1 (
		.dest_out(READLINEMANY_sync), // 1-bit output: src_in synchronized to the destination clock domain. This output is
                           // registered.

		.dest_clk(s00_axi_ctrl_aclk), // 1-bit input: Clock signal for the destination clock domain.
		.src_clk(1'b0),   // 1-bit input: optional; required when SRC_INPUT_REG = 1
		.src_in(READLINEMANY)      // 1-bit input: Input signal to be synchronized to dest_clk domain.
	);

    ADCDataSampler # (
    	.C_ADC_DATA_WIDTH(C_ADC_DATA_WIDTH),
        .C_REGISTER_LINES_WIDTH(C_REGISTER_LINES_WIDTH)
    ) ADCDataSampler_i (
    	.aclk   (s00_axi_ctrl_aclk),
    	.aresetn(s00_axi_ctrl_aresetn),

    	.read_line(register_cr4[0] | READLINEONE_sync),
    	.read_line_many  (register_cr4[1] | READLINEMANY_sync),
    	.cis_si   (CIS_SI),
    	.active_line_end(active_line_end),
    	
    	.st0            (st0),
    	.st1            (st1),
    	.ADC_DATA       (input_data),
    	
    	.full_pixel_count(full_pixel_count),
        .register_lines          (register_lines),
        .register_lines_decrement(register_lines_decrement),

    	.m00_axis_aclk   (m00_axis_aclk),
    	.m00_axis_aresetn(m00_axis_aresetn),
    	.m00_axis_tvalid (m00_axis_tvalid),
    	.m00_axis_tdata  (m00_axis_tdata),
    	.m00_axis_tstrb  (m00_axis_tstrb),
    	.m00_axis_tlast  (m00_axis_tlast),
    	.m00_axis_tready (m00_axis_tready)
    );

    wire [C_INTERRUPT_WIDTH-1:0] interrupt;

    Interrupt irqline_i (
    	.aclk            (s00_axi_ctrl_aclk),
    	.aresetn         (s00_axi_ctrl_aresetn),
    	.interrupt_enable(INTERRUPT_ENABLE[0]),
    	.interrupt_ack   (INTERRUPT_ACK[0]),
    	.interrupt_status(INTERRUPT_STATUS[0]),
    	.interrupt       (interrupt[0]),
    	.input_strobe    (register_lines_decrement)
	);

    Interrupt irqframe_i (
    	.aclk            (s00_axi_ctrl_aclk),
    	.aresetn         (s00_axi_ctrl_aresetn),
    	.interrupt_enable(INTERRUPT_ENABLE[1]),
    	.interrupt_ack   (INTERRUPT_ACK[1]),
    	.interrupt_status(INTERRUPT_STATUS[1]),
    	.interrupt       (interrupt[1]),
    	.input_strobe    (register_lines_decrement & (register_lines == 1))
	);

    reg irq_dff;
    always @(posedge s00_axi_ctrl_aclk)
    	if (s00_axi_ctrl_aresetn == 0)
    		irq_dff <= 0;
    	else
    		irq_dff <= (|interrupt) & GIER;

    assign irq = irq_dff;
    
	// User logic ends
	endmodule
