module Interrupt ( 
	input wire aclk,
	input wire aresetn,
	input wire interrupt_enable,
	input wire interrupt_ack,	
	output bit interrupt_status,
	output wire interrupt,

	input wire input_strobe	
);

	always_ff @(posedge aclk)
		if (aresetn == '0) begin
			interrupt_status <= '0;
		end
		else begin
			if (input_strobe)
				interrupt_status <= '1;
			if (interrupt_ack)
				interrupt_status <= '0;
		end

	assign interrupt = interrupt_status & interrupt_enable;
endmodule : Interrupt
