
module PRBS # 
	(
		parameter PRBS_OUT_WIDTH = 8
	)
	(
		input wire aclk,
		input wire aresetn,
		input wire cis_si,
		input wire adc_clk,

		output wire [PRBS_OUT_WIDTH - 1 : 0] prbs_out
	);


wire clock_rise;
wire clock_fall;
	
	edge_detect ed (
		.clk (aclk),
		.in  (adc_clk),
		.rise(clock_rise),
		.fall(clock_fall)
	);
	
	
bit [6:0] d;
bit a;
	always_ff @(posedge aclk) begin
		if ((~aresetn) | cis_si) begin
			d <= '1;
			a <= 0;
		end
		else begin
			if (clock_fall | clock_rise) begin
    			d <= { d[5:0], d[6] ^ d[5] };
    			a <= ~a;
    		end
    	end
	end
	
	assign prbs_out = {1'b0, d};

endmodule
