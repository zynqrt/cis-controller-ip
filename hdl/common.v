module synchronizer(clk, nrst, sigin, sigout);
	input clk;
	input nrst;
	input sigin;
	output wire sigout;

	reg a, b;

	always @(posedge clk or negedge nrst) begin
		if (!nrst) begin
			a <= 1'b0;
			b <= 1'b0;
		end
		else begin
			a <= sigin;
			b <= a;
		end
	end

	assign sigout = b;
endmodule


/* Детектор фронтов */
module edge_detect(clk, in, rise, fall);
input clk;

input in;
output rise, fall;

reg dff;
	always @(posedge clk)
		dff <= in;
	
wire rise;
wire fall;
assign rise = in & (!dff);
assign fall = (!in) & dff;

endmodule
