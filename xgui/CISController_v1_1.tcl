# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  ipgui::add_page $IPINST -name "Page 0"

  set INACTIVE_PIXELS [ipgui::add_param $IPINST -name "INACTIVE_PIXELS"]
  set_property tooltip {Inactive Pixels from DS} ${INACTIVE_PIXELS}
  set ACTIVE_PIXELS_300DPI [ipgui::add_param $IPINST -name "ACTIVE_PIXELS_300DPI"]
  set_property tooltip {Active Pixels from DS +2, 300dpi} ${ACTIVE_PIXELS_300DPI}
  set ACTIVE_PIXELS_600DPI [ipgui::add_param $IPINST -name "ACTIVE_PIXELS_600DPI"]
  set_property tooltip {Active Pixels from DS +2, 600dpi} ${ACTIVE_PIXELS_600DPI}
  set ACTIVE_PIXELS_1200DPI [ipgui::add_param $IPINST -name "ACTIVE_PIXELS_1200DPI"]
  set_property tooltip {Active Pixels from DS +2, 1200dpi} ${ACTIVE_PIXELS_1200DPI}

}

proc update_PARAM_VALUE.ACTIVE_PIXELS_1200DPI { PARAM_VALUE.ACTIVE_PIXELS_1200DPI } {
	# Procedure called to update ACTIVE_PIXELS_1200DPI when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ACTIVE_PIXELS_1200DPI { PARAM_VALUE.ACTIVE_PIXELS_1200DPI } {
	# Procedure called to validate ACTIVE_PIXELS_1200DPI
	return true
}

proc update_PARAM_VALUE.ACTIVE_PIXELS_300DPI { PARAM_VALUE.ACTIVE_PIXELS_300DPI } {
	# Procedure called to update ACTIVE_PIXELS_300DPI when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ACTIVE_PIXELS_300DPI { PARAM_VALUE.ACTIVE_PIXELS_300DPI } {
	# Procedure called to validate ACTIVE_PIXELS_300DPI
	return true
}

proc update_PARAM_VALUE.ACTIVE_PIXELS_600DPI { PARAM_VALUE.ACTIVE_PIXELS_600DPI } {
	# Procedure called to update ACTIVE_PIXELS_600DPI when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ACTIVE_PIXELS_600DPI { PARAM_VALUE.ACTIVE_PIXELS_600DPI } {
	# Procedure called to validate ACTIVE_PIXELS_600DPI
	return true
}

proc update_PARAM_VALUE.C_ADC_DATA_WIDTH { PARAM_VALUE.C_ADC_DATA_WIDTH } {
	# Procedure called to update C_ADC_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_ADC_DATA_WIDTH { PARAM_VALUE.C_ADC_DATA_WIDTH } {
	# Procedure called to validate C_ADC_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M00_AXIS_TDATA_WIDTH { PARAM_VALUE.C_M00_AXIS_TDATA_WIDTH } {
	# Procedure called to update C_M00_AXIS_TDATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M00_AXIS_TDATA_WIDTH { PARAM_VALUE.C_M00_AXIS_TDATA_WIDTH } {
	# Procedure called to validate C_M00_AXIS_TDATA_WIDTH
	return true
}

proc update_PARAM_VALUE.INACTIVE_PIXELS { PARAM_VALUE.INACTIVE_PIXELS } {
	# Procedure called to update INACTIVE_PIXELS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.INACTIVE_PIXELS { PARAM_VALUE.INACTIVE_PIXELS } {
	# Procedure called to validate INACTIVE_PIXELS
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CTRL_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_CTRL_DATA_WIDTH } {
	# Procedure called to update C_S00_AXI_CTRL_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CTRL_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_CTRL_DATA_WIDTH } {
	# Procedure called to validate C_S00_AXI_CTRL_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CTRL_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_CTRL_ADDR_WIDTH } {
	# Procedure called to update C_S00_AXI_CTRL_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CTRL_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_CTRL_ADDR_WIDTH } {
	# Procedure called to validate C_S00_AXI_CTRL_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CTRL_BASEADDR { PARAM_VALUE.C_S00_AXI_CTRL_BASEADDR } {
	# Procedure called to update C_S00_AXI_CTRL_BASEADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CTRL_BASEADDR { PARAM_VALUE.C_S00_AXI_CTRL_BASEADDR } {
	# Procedure called to validate C_S00_AXI_CTRL_BASEADDR
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CTRL_HIGHADDR { PARAM_VALUE.C_S00_AXI_CTRL_HIGHADDR } {
	# Procedure called to update C_S00_AXI_CTRL_HIGHADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CTRL_HIGHADDR { PARAM_VALUE.C_S00_AXI_CTRL_HIGHADDR } {
	# Procedure called to validate C_S00_AXI_CTRL_HIGHADDR
	return true
}


proc update_MODELPARAM_VALUE.C_S00_AXI_CTRL_DATA_WIDTH { MODELPARAM_VALUE.C_S00_AXI_CTRL_DATA_WIDTH PARAM_VALUE.C_S00_AXI_CTRL_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_CTRL_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_CTRL_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S00_AXI_CTRL_ADDR_WIDTH { MODELPARAM_VALUE.C_S00_AXI_CTRL_ADDR_WIDTH PARAM_VALUE.C_S00_AXI_CTRL_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_CTRL_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_CTRL_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M00_AXIS_TDATA_WIDTH { MODELPARAM_VALUE.C_M00_AXIS_TDATA_WIDTH PARAM_VALUE.C_M00_AXIS_TDATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M00_AXIS_TDATA_WIDTH}] ${MODELPARAM_VALUE.C_M00_AXIS_TDATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_ADC_DATA_WIDTH { MODELPARAM_VALUE.C_ADC_DATA_WIDTH PARAM_VALUE.C_ADC_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_ADC_DATA_WIDTH}] ${MODELPARAM_VALUE.C_ADC_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.INACTIVE_PIXELS { MODELPARAM_VALUE.INACTIVE_PIXELS PARAM_VALUE.INACTIVE_PIXELS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.INACTIVE_PIXELS}] ${MODELPARAM_VALUE.INACTIVE_PIXELS}
}

proc update_MODELPARAM_VALUE.ACTIVE_PIXELS_1200DPI { MODELPARAM_VALUE.ACTIVE_PIXELS_1200DPI PARAM_VALUE.ACTIVE_PIXELS_1200DPI } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ACTIVE_PIXELS_1200DPI}] ${MODELPARAM_VALUE.ACTIVE_PIXELS_1200DPI}
}

proc update_MODELPARAM_VALUE.ACTIVE_PIXELS_600DPI { MODELPARAM_VALUE.ACTIVE_PIXELS_600DPI PARAM_VALUE.ACTIVE_PIXELS_600DPI } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ACTIVE_PIXELS_600DPI}] ${MODELPARAM_VALUE.ACTIVE_PIXELS_600DPI}
}

proc update_MODELPARAM_VALUE.ACTIVE_PIXELS_300DPI { MODELPARAM_VALUE.ACTIVE_PIXELS_300DPI PARAM_VALUE.ACTIVE_PIXELS_300DPI } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ACTIVE_PIXELS_300DPI}] ${MODELPARAM_VALUE.ACTIVE_PIXELS_300DPI}
}

